<?php

/**
 * @file
 * Hooks specific to the Crowdriff API module.
 */

/**
 * @addtogroup hooks
 */

/**
 * Alter Crowdriff assets.
 *
 * This hook is invoked from the CrowdriffService class.
 *
 * Crowdriff assets and their data can be altered.
 *
 * @param array $assets
 *   The array of assets to alter.
 */
function hook_crowdriff_api_alter_assets(array &$assets) {
}
