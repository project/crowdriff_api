<?php

namespace Drupal\crowdriff_api\Form;

use Drupal\Core\Cache\CacheFactoryInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Crowdriff Config Form.
 *
 * @package Drupal\crowdriff_api\Form
 */
class CrowdriffConfigForm extends ConfigFormBase {

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  private $cache;

  /**
   * Crowdriff Config Form constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Cache\CacheFactoryInterface $cache_factory
   *   The cache factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory, CacheFactoryInterface $cache_factory) {
    parent::__construct($config_factory);
    $this->cache = $cache_factory->get('crowdriff');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('cache_factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'crowdriff_api.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'crowdriff_api_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('crowdriff_api.settings');

    $form['api'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('API Settings'),
    ];

    $key_collection_url = Url::fromRoute('entity.key.collection')->toString();
    $form['api']['keys'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Keys'),
      '#description' => $this->t('Use keys managed by the key module. <a href=":keys">Manage keys</a>', [
        ':keys' => $key_collection_url,
      ]),
      '#tree' => FALSE,
    ];

    $form['api']['keys']['api_key_name'] = [
      '#type' => 'key_select',
      '#title' => $this->t('Crowdriff API Key'),
      '#description' => $this->t('The Crowdriff API Key.'),
      '#empty_option' => $this->t('- Select Key -'),
      '#default_value' => $config->get('api_key_name'),
      '#key_filters' => ['type' => 'authentication'],
      '#required' => TRUE,
    ];

    $form['api']['api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Crowdriff API URL'),
      '#description' => $this->t('Set API url endpoint.'),
      '#default_value' => !empty($config->get('api_url')) ? $config->get('api_url') : 'https://api.crowdriff.com/v2',
      '#required' => TRUE,
    ];

    $form['caching'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Cache Settings'),
    ];

    $form['caching']['cache'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Crowdriff Caching'),
      '#description' => $this->t('Enable/disable caching for Crowdriff.'),
      '#default_value' => !empty($config->get('cache')) ? $config->get('cache') : 0,
    ];

    $form['caching']['cache_length'] = [
      '#type' => 'number',
      '#min' => 0,
      '#step' => 1,
      '#title' => $this->t('Cache Length in minutes'),
      '#description' => $this->t('Length of time in minutes to cache API requests. A length of 0 disables cache.'),
      '#default_value' => !empty($config->get('cache_length')) ? $config->get('cache_length') : 0,
      '#states' => [
        'visible' => [
          ':input[name="cache"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    // Clear cache.
    $this->cache->invalidateAll();
    // Set config.
    $this->config('crowdriff_api.settings')
      ->set('api_key_name', $form_state->getValue('api_key_name'))
      ->set('api_url', $form_state->getValue('api_url'))
      ->set('cache', $form_state->getValue('cache'))
      ->set('cache_length', $form_state->getValue('cache_length'))
      ->save();
  }

}
