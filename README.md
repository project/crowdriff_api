# Crowdriff API

Module provides base functionality to interact with Crowdriff
API service and to configure API authorization for your site.

You will need to extend this module to actually output or capture
any data from Crowdriff API itself.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/crowdriff_api).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/crowdriff_api).


## Requirements

- Drupal 9 or 10
- Crowdriff API Key/Token is required.


## Required Modules

- [Key](https://www.drupal.org/project/key)


### Supported API Endpoints

**Albums & Folders**

- Retrieve Albums by IDs - https://api.crowdriff.com/v2/albums/{album_ids}
- List all albums - https://api.crowdriff.com/v2/albums
- Retrieve Folders by IDs - https://api.crowdriff.com/v2/folders/{folder_ids}
- List all folders - https://api.crowdriff.com/v2/folders

**Apps**

- Retrieve apps by IDs - https://api.crowdriff.com/v2/apps/{app_ids}
- List all apps - https://api.crowdriff.com/v2/apps

**Assets**

- Search - https://api.crowdriff.com/v2/search
- Retrieve Assets by IDs - https://api.crowdriff.com/v2/assets/{uuids}
- Asset Analytics - https://api.crowdriff.com/v2/assets/{uuid}/analytics
- List Assets in Gallery - https://api.crowdriff.com/v2/galleries/{gallery_id}/assets

**CTAS**

- List all CTAs - https://api.crowdriff.com/v2/ctas
- Retrieve CTAs by IDs - https://api.crowdriff.com/v2/ctas/{cta_ids}
- CTA Analytics - https://api.crowdriff.com/v2/ctas/{id}/analytics


## Optional/Noted Modules

#### Media Library Crowdriff

Module provides plugins that extend Media library to pull in
Crowdriff assets as media entities. Provides custom media entities
for Crowdriff images and videos. Relies on the Crowdriff API module.

- [media_library_extend_crowdriff]
(https://www.drupal.org/project/media_library_extend_crowdriff)


## Installation/Setup

Require package/module:

```bash
composer require drupal/crowdriff_api
```

Install and enable module via drush.

```bash
drush en crowdriff_api
drush cr
```

Crowdriff API settings can be managed from here:
`admin/config/services/crowdriff`


## Code Usage/Examples

All methods and functionality are found in the CrowdriffService class.

```php
/** @var \Drupal\crowdriff_api\CrowdriffService $crowdriffService */
$crowdriffService = \Drupal::service('crowdriff_api.crowdriff_service');

// Get the API key.
$api_key = $crowdriffService->getApiKey();

// Get the API url.
$api_url = $crowdriffService->getApiUrl();

// Get folders.
$data = $crowdriffService->getFolders();

// Get apps.
$data = $crowdriffService->getApps();

// Get albums.
$data = $crowdriffService->getAlbums();

// Get ctas.
$data = $crowdriffService->getCtas();

// Get assets by ids.
$asset_ids = 'ig-1234,tw-1234';
$data = $crowdriffService->getAssetsById($asset_ids);

// Get asset analytics.
$asset_id = 'ig-1234';
$data = $crowdriffService->getAssetAnalytics($asset_id);

// Get albums from folder.
$folder_id = 'folder-123';
$albums = $crowdriffService->getAlbumsFromFolder($folder_id);

// Get assets from albums.
$album_ids = ['album-123'];
$assets = $crowdriffService->getAssetsFromAlbums($album_ids);

// Get assets from folder.
$folder_id = 'folder-123';
$assets = $crowdriffService->getAssetsFromFolder($folder_id);

// Get assets from app.
$app_id = 'app-123';
$assets = $crowdriffService->getAssetsFromApp($app_id);

// General/custom API request.
$data = $crowdriffService->makeRequest($path, $cache_key, $method, $params, $paging_key);
```

## Maintainers

- George Anderson - [geoanders](https://www.drupal.org/u/geoanders)
- Michael O'Hara - [mikeohara](https://www.drupal.org/u/mikeohara)
